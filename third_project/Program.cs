﻿using System.Linq.Expressions;

static int Hola(Usuario x) { return x.getId; }

Console.WriteLine("hola");

static void MensajePrincipal()
{
    Random randN = new Random();
    randN.Next(1, 10);
    Console.WriteLine("En este sistema podremos ver cosas");
    Console.WriteLine("1.- Ingresar nuevo usuario");
    Console.WriteLine("2.- Ver datos");
    Console.WriteLine("3.- Retirar dinero");
    Console.WriteLine("4.- Salir");
}

//-------- main ---------
Usuario[] usuarios = new Usuario[25];

Console.WriteLine("Bienvenido al sistema del banco");

int numUsers = 0;

int sk = 1;
do
{
    MensajePrincipal();
    Console.Write("Ingresa una opcion: ");
    int opcion = Convert.ToInt32(Console.ReadLine());
    switch (opcion)
    {
        case 1:
            Console.Write("Ingresa el id: ");
            int val1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Ingresa el nombre: ");
            string? val2 = Console.ReadLine();

            usuarios[numUsers] = new Usuario(val1, val2 ?? "", 20000);
            numUsers++;
            break;
        case 2:
            Console.Write("Ingresa el nombre de contacto a buscar: ");
            string? valb = Console.ReadLine();

            try{
                Console.WriteLine(usuarios[0].getNombre);
                for (int i = 0; i < numUsers; i++)
                {
                    if (valb == usuarios[i].getNombre)
                    {
                        Console.WriteLine($"{usuarios[i].getNombre} | {usuarios[i].getId}");
                    }
                } 
            } catch (Exception) {
                Console.WriteLine("No hay usuarios");
            }
            break;
        case 3:
            Console.Write("Ingresa el usuario del cual extraer el dinero: ");
            string? valr = Console.ReadLine();
            for (int i = 0; i < numUsers; i++)
            {
                if (valr == usuarios[i].getNombre)
                {
                    Console.Write("cuanto dinero vas a sacar: ");
                    double din = Convert.ToDouble(Console.ReadLine());
                    usuarios[i].getDinero = usuarios[i].getDinero - din;
                    Console.WriteLine($"Nuevo valor {usuarios[i].getDinero}");
                }
            }
            break;
        case 4:
            sk = 0;
            break;
    }
} while (sk != 0);

public class Usuario
{
    private int id;
    private string? nombre;
    private double dinero;

    public Usuario(int idPa, string nombrePa, double dineroPa)
    {
        this.id = idPa;
        this.nombre = nombrePa;
        this.dinero = dineroPa;
    }

    public int getId
    {
        get { return this.id; }
        set { id = value; }
    }

    public string getNombre
    {
        get { return this.nombre ?? ""; }
        set { nombre = value; }
    }

    public double getDinero
    {
        get { return this.dinero; }
        set { dinero = value; }
    }
}

