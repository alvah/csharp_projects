﻿// See https://aka.ms/new-console-template for more information

Cositas<string> val1 = new Cositas<string>("s");
Person val2 = new Person(2, "gola");
Trabajador val3 = new Trabajador();

val3.DecirHola();

class Cositas<T>
{
  private T nombre;

  public Cositas(T nombrePa)
  {
    this.nombre = nombrePa;
  }
}

class Person
{
  private int edad;
  private string nombre;

  public Person(int x, string y)
  {
    this.edad = x;
    this.nombre = y;
  }
}

abstract class Humano
{
  public void DecirHola()
  {
    Console.WriteLine("hola");
  }
}

class Trabajador : Humano
{
  private int val;

  public Trabajador()
  {
    this.val = 2;
  }
}
