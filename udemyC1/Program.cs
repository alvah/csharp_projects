﻿MyList<int> list = new MyList<int>(10);
list.Add(2);
list.Add(3);

MyList<People> personas = new MyList<People>(10);

personas.Add(new People("Juan", 20));

Console.WriteLine(list.GetElement(11));


Shark[] sharks = new Shark[]{
  new Shark("Tiburon", 10),
  new Shark("Tiburoncin", 20),
};

static void ShowFish(IFish[] fishes)
{
  foreach (var fish in fishes)
  {
    Console.WriteLine(fish.Swim());
  }
}

ShowFish(sharks);

class People
{
  private string _name;
  private int _age;

  public People(string name, int age)
  {
    _name = name;
    _age = age;
  }

  public string getName { get => _name; set => _name = value; }
  public int getAge { get => _age; set => _age = value; }

  public string getInfo()
  {
    return $"Name: {_name}, Age: {_age}";
  }

  public override string ToString()
  {
    return $"Name: {_name} | Age: {_age}";
  }
}

class Doctor : People
{

  private string _specialty;
  public Doctor(string name, int age, string specialty) : base(name, age)
  {
    _specialty = specialty;
  }

  public string getSpecialty { get => _specialty; set => _specialty = value; }
}

public class A
{
  public virtual string Hi()
  {
    return "Hola soy A";
  }
}

public class B : A
{
  public override string Hi()
  {
    return base.Hi() + " Hola soy B";
  }
}

class Sale
{
  private decimal[] _amounts;
  private int _n;
  private int _end;

  public Sale(int n)
  {
    _amounts = new decimal[n];
    _n = n;
    _end = 0;
  }

  public void Add(decimal amount)
  {
    if (_end < _n)
    {
      _amounts[_end] = amount;
      _end++;
    }
  }

  public virtual decimal getTotal()
  {
    decimal total = 0;
    for (int i = 0; i < _end; i++)
    {
      total += _amounts[i];
    }
    return total;
  }

}

class SaleWithTax : Sale
{
  private decimal _taxRate;

  public SaleWithTax(int n, decimal taxRate) : base(n)
  {
    _taxRate = taxRate;
  }

  public override decimal getTotal()
  {
    decimal saleAmount = base.getTotal();
    return saleAmount + (saleAmount * _taxRate);
  }
}

public class Shark : IAnimal, IFish
{
  public string Name { get; set; }
  public int Speed { get; set; }

  public Shark(string name, int speed)
  {
    Name = name;
    Speed = speed;
  }


  public string Swim()
  {
    return "Swimming";
  }
}

public interface IAnimal
{
  public string Name { get; set; }
}

public interface IFish
{
  public int Speed { get; set; }
  public string Swim();
}


class MyList<T>
{
  private T[] _elements;
  private int _index = 0;

  public MyList(int n)
  {
    _elements = new T[n];
  }

  public void Add(T element)
  {
    if (_index < _elements.Length)
    {
      _elements[_index] = element;
      _index++;
    }
  }

  public T GetElement(int i)
  {
    if (i < _elements.Length && i >= 0)
    {
      return _elements[i];
    }

    return default(T) ?? throw new Exception("Index out of range");
  }

  public string GetString()
  {
    string result = "";
    foreach (var element in _elements)
    {
      result += element.ToString() + " ";
    }
    return result;
  }

}


