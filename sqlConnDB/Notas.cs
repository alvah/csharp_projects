using Dapper;

class Nota : Connection
{
  public void ListNotas()
  {
    var conn = establishConnection();

    var notas = conn.Query("SELECT * FROM nota");
    foreach (var nota in notas)
    {
      Console.WriteLine(nota.id_nota);
    }
    conn.Close();
  }

  public void NewNote(string titulo, string contenido)
  {
    var conn = establishConnection();

    var sql = $"INSERT INTO nota (titulo, contenido) VALUES (@titulo, @contenido)";

    var notaA = new { titulo = titulo, contenido = contenido };
    conn.Execute(sql, notaA);
    conn.Close();

  }

  public void DeleteNota(int n)
  {
    var conn = establishConnection();

    var sql = $"DELETE FROM nota WHERE id_nota = @id_nota";

    var notaA = new { id_nota = n };
    conn.Execute(sql, notaA);
    conn.Close();
  }
}
