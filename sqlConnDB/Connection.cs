using Npgsql;


class Connection
{
  NpgsqlConnection conn = new NpgsqlConnection();
  private static string connString = "Host=192.168.1.97;Username=raul;Password=1234;Database=notasdb";

  public NpgsqlConnection establishConnection()
  {
    try
    {
      conn.ConnectionString = connString;
      Console.WriteLine("Connection established");
      conn.Open();
    }
    catch (Exception e)
    {
      Console.WriteLine(e.Message);
    }
    return conn;
  }
}
