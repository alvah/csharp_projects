﻿// See https://aka.ms/new-console-template for more information

List<Beer> beers = new List<Beer>()
{
  new Beer(){
  Name = "Corona", Country = "Mexico"
},
new Beer()
{
  Name = "Budweiser",
  Country = "USA",
},
new Beer()
{
  Name = "Heineken",
  Country = "Netherlands",
}
};

foreach (var beer in beers)
{
  Console.WriteLine(beer);
}

Console.WriteLine("------------");

var beersName = from b in beers
                where b.Country == "Mexico"
                select b;
class Beer
{
  public string? Name { get; set; }
  public string? Country { get; set; }

  public override string ToString()
  {
    return $"{Name} from {Country}";
  }
}
