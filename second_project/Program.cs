﻿using Npgsql;

string connString = "Host=192.168.1.121;Username=usuario1;Password=1234;Database=midb";
var dataSourceBuilder = new NpgsqlDataSourceBuilder(connString);
var dataSource = dataSourceBuilder.Build();
var conn = await dataSource.OpenConnectionAsync();
Console.WriteLine(conn);



Console.WriteLine("1- Agregar\n2- Ver");
Console.Write("Ingresa una opción: ");
int num = Convert.ToInt32(Console.ReadLine());

if (num == 1)
{
    UpdateDb(conn);
}
else if (num == 2)
{
    ShowDb(conn);
}

async void UpdateDb(NpgsqlConnection connPa)
{
    Console.WriteLine("A quien vas a agregar: ");
    string? val = Console.ReadLine();
    await using (var command = new NpgsqlCommand("INSERT INTO estudiantes (nombre) VALUES (@P)", connPa))
    {
        command.Parameters.AddWithValue("p", val ?? "");
        Console.WriteLine("Se agregó un valor");
        await command.ExecuteNonQueryAsync();
    }
}

async void ShowDb(NpgsqlConnection connPa)
{
    await using (var command = new NpgsqlCommand("SELECT * FROM estudiantes", connPa))
    await using (var reader = await command.ExecuteReaderAsync())
    {
        while (await reader.ReadAsync())
        {
            Console.WriteLine(reader.GetString(1));
        }
    }
}

/*await using (var command = new NpgsqlCommand("SELECT * FROM estudiantes", conn))
  await using (var reader = await command.ExecuteReaderAsync()){
    while (await reader.ReadAsync()){
      Console.WriteLine(reader.GetString(1));
    }
  }
*/

