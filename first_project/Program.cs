﻿// See https://aka.ms/new-console-template for more information

Estudiante[] estudiantes = new Estudiante[2];
Console.WriteLine($"La cantidad de estudiantes es: {estudiantes.Length}");

for (int i = 0; i < estudiantes.Length; i++)
{
    Console.Write("Ingresa la edad: ");
    int edad = Convert.ToInt32(Console.ReadLine());

    Console.Write("Ingresa el promedio: ");
    double promedio = Convert.ToDouble(Console.ReadLine());

    Console.Write("Ingresa el nombre: ");
    string? nombre = Console.ReadLine();

    Console.Write("Ingresa el apellido: ");
    string? apellido = Console.ReadLine();

    estudiantes[i] = new Estudiante(edad, promedio, nombre!, apellido!);
    estudiantes[i].MostrarDatos();
}

public class Estudiante
{
    private int edad;
    private double promedio;
    private string nombre,
        apellido;

    public Estudiante(int ed, double pr, string no, string ap)
    {
        this.edad = ed;
        this.promedio = pr;
        this.nombre = no;
        this.apellido = ap;
    }

    public void MostrarDatos()
    {
        Console.WriteLine($"La edad del estudiante {this.nombre} es: {this.edad}");
    }
}
