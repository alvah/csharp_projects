﻿List<Teacher> listaTeachers = new List<Teacher>();

listaTeachers.Add(new Teacher("121",23,"asa"));

bool bucle = true;

while(bucle){
    Console.WriteLine("Ingresa un nombre");
    string? nombre = Console.ReadLine();
    Console.WriteLine("Ingresa un numero");
    int edad = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine("Ingresa una profesion");
    string? profesion = Console.ReadLine();

    Teacher teacherx = new Teacher(nombre ?? "", edad, profesion ?? "");
    listaTeachers.Add(teacherx);

    bucle = false;
}

foreach (Teacher elem in listaTeachers){
    Console.WriteLine(elem.mostrarDatos());
}

class Person{
    private string _name;
    private int _age;

    public Person(string name, int age){
        _name = name;
        _age = age;
    }

    public string getName { get => _name; set => _name = value;}
    public int getAge { get => _age; set => _age = value;}

    public virtual string mostrarDatos(){
        return $"hola {_name}";
    }
}

class Teacher : Person{
    private string _profesion;

    public Teacher(string name, int age, string profesion) : base(name,age){
        _profesion = profesion;
    }

    public string getProfesion { get => _profesion; set => _profesion = value;}

    public override string mostrarDatos(){
        return base.mostrarDatos() + "hola";
    }
}
