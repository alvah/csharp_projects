﻿Persona persona1 = new(1, "s");

for (int i = 0; i < 10; i++)
{
    Console.WriteLine(i);
}

for (int i = 0; i < 10; i++)
{
    Console.WriteLine(i);
}


public class Persona
{
    private int numero;
    private string nombre;

    public Persona(int num, string name)
    {
        this.numero = num;
        this.nombre = name;
    }

    public int Numero { get => this.numero; set => this.numero = value; }
    public string Nombre { get => this.nombre; set => this.nombre = value; }
}

